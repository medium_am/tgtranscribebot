FROM python:3.10-alpine
RUN mkdir /app
WORKDIR /app
COPY pyproject.toml /app
RUN apk add --update --no-cache\
    build-base jpeg-dev zlib-dev libjpeg\
    gettext\
    py3-lxml\
    py3-pillow\
    openldap-dev\
    python3-dev\
    && rm -rf /var/cache/apk/*
RUN pip3 install poetry && poetry config virtualenvs.create false && poetry install --no-dev
COPY . /app
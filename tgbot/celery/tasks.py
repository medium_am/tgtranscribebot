import asyncio
import math
import logging

from .celery import app
from tgbot.misc import bot
from tgbot.ydxtranscribe.utils import recognize, check_operation
from tgbot.config import config


async def send_transcribed_text(text: str, chat_id: int, reply_message_id: int):
    await bot.send_message(chat_id=chat_id, reply_to_message_id=reply_message_id, text=text)


def prepare_text(result: dict):
    text = ''
    for i in result["response"]["chunks"]:
        if text == '':
            text = i["alternatives"][0]["text"]
        else:
            text += f'. {i["alternatives"][0]["text"]}'
    return text


async def recognize_and_wait_result(obj_key: str, duration: int, chat_id: int, reply_message_id: int):
    estimated_time = math.ceil(duration/6)
    s3_url_file = f"https://storage.yandexcloud.net/{config.ydx.s3_bucket}/{obj_key}"
    try:
        resp = await recognize(s3_url_file)
        await asyncio.sleep(estimated_time)
        result = await check_operation(resp["id"])
        while result["done"] is False:
            result = await check_operation(resp["id"])
            if result["done"] is False:
                await asyncio.sleep(2)
        await send_transcribed_text(prepare_text(result), chat_id, reply_message_id)
    except Exception:
        logging.error("Something went wrong with transcribation")
        await send_transcribed_text("Something went wrong. Cannot transcribe", chat_id, reply_message_id)


@app.task
def transcribe(obj_key: str, duration: int, chat_id: int, reply_message_id: int):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(recognize_and_wait_result(obj_key, duration, chat_id, reply_message_id))

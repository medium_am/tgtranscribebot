from celery import Celery

from tgbot.config import config


app = Celery('celery',
             broker=config.celeryconf.celery_broker_url,
             include=['tgbot.celery.tasks'])

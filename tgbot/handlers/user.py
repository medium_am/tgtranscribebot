import io
import math
import uuid
import logging

from aiogram import Dispatcher
from aiogram import types

from tgbot.ydxtranscribe.storage import upload_obj
from tgbot.celery.tasks import transcribe


async def user_start(message: types.Message):
    await message.reply(f"Hello, {message['from']['first_name']}! Just send me voice")


async def echo_handler(message: types.Message):
    text = [
        "This isn't voice message.",
        "Please send voice message or forward voice message to me."
    ]
    await message.answer('\n'.join(text))


async def voice_handler(message: types.Message):
    obj_key = f"{uuid.uuid4().hex[:10]}.ogg"
    buf = io.BytesIO()
    duration = message.voice.duration
    if duration > 600:
        await message.answer("Cannot transcribe audio with duration above then 10 min")
    else:
        voice = await message.voice.get_file()
        await voice.download(destination_file=buf)
        try:
            await upload_obj(buf, obj_key=obj_key)
            logging.info(f"File {obj_key} from {message.from_id} was upload")
            transcribe.delay(obj_key, duration, message.chat.id, message.message_id)
            await message.answer(f"Transcribation started. Estimated execution time - {math.ceil(duration/6)} sec")
        except Exception as e:
            logging.error(e)
            await message.answer("Something went wrong")


def register_user(dp: Dispatcher):
    dp.register_message_handler(user_start, commands=["start"], state="*")
    dp.register_message_handler(echo_handler, content_types=types.ContentType.TEXT)
    dp.register_message_handler(voice_handler, content_types=types.ContentType.VOICE)

from dataclasses import dataclass
from pathlib import Path

from environs import Env

BASE_DIR = Path(__file__).parent.resolve()


@dataclass
class TgBot:
    token: str
    admin_ids: list[int]


@dataclass
class Ydx:
    api_key: str
    s3_access_key: str
    s3_secret_key: str
    s3_region: str
    s3_bucket: str


@dataclass
class CeleryConf:
    celery_broker_url: str


@dataclass
class Config:
    tg_bot: TgBot
    ydx: Ydx
    celeryconf: CeleryConf


def load_config(path: Path = None):
    env = Env()
    env.read_env(path)

    return Config(
        tg_bot=TgBot(
            token=env.str("BOT_TOKEN"),
            admin_ids=list(map(int, env.list("ADMINS")))
        ),
        ydx=Ydx(
            api_key=env.str("YDX_API_KEY"),
            s3_access_key=env.str("YDX_S3_ACCESS_KEY"),
            s3_secret_key=env.str("YDX_S3_SECRET_KEY"),
            s3_region=env.str("YDX_S3_REGION"),
            s3_bucket=env.str("YDX_S3_BUCKET")
        ),
        celeryconf=CeleryConf(
            celery_broker_url=env.str("CELERY_BROKER_URL")
        )
    )


config = load_config(BASE_DIR / ".env")

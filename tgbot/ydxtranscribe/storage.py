import aioboto3
from io import BytesIO

from tgbot.misc import config


session = aioboto3.Session(
    aws_access_key_id=config.ydx.s3_access_key,
    aws_secret_access_key=config.ydx.s3_secret_key,
    region_name=config.ydx.s3_region,

)


async def upload_obj(file: BytesIO, obj_key: str):
    async with session.client("s3", endpoint_url='https://storage.yandexcloud.net') as s3:
        await s3.upload_fileobj(file, config.ydx.s3_bucket, obj_key)

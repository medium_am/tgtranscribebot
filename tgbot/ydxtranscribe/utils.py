import aiohttp
import json

from tgbot.config import config

headers = {
    'Authorization': f"Api-Key {config.ydx.api_key}",
    'Content-Type': 'application/json'
}


async def recognize(s3_file_url: str):
    async with aiohttp.ClientSession(headers=headers) as session:
        ydx_transcribe_url = 'https://transcribe.api.cloud.yandex.net/speech/stt/v2/longRunningRecognize'
        data = {
            "config": {
                "specification": {
                    "languageCode": "ru-RU",
                    "model": "general",
                }
            },
            "audio": {
                "uri": s3_file_url
            }
        }
        async with session.post(ydx_transcribe_url, data=json.dumps(data)) as resp:
            result = await resp.json()
            return result


async def check_operation(operation_id: str):
    async with aiohttp.ClientSession(headers=headers) as session:
        ydx_operation_check_url = f'https://operation.api.cloud.yandex.net/operations/{operation_id}'
        async with session.get(ydx_operation_check_url) as resp:
            result = await resp.json()
            return result
